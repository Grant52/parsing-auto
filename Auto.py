# -*- coding: utf-8 -*-
import requests
import json


class Auto:
    def __init__(self, mark_model, class_auto):
        self.mark_model = mark_model
        self.class_auto = class_auto

vehClass = {'1': 'MINI',
            '3': 'ECONOMY',
            '4': 'COMPACT',
            '6': 'INTERMEDIATE',
            '7': 'STANDARD',
            '8': 'FULLSIZE',
            '9': 'LUXURY',
            '10': 'PREMIUM',
            '15': 'CARGOVAN',
            '11': 'MINIVAN',
            '43': 'CONVERTIBLE',
            '44': 'ESTATE',
            '45': '5SEATCARRIER',
            '46': '7SEATCARRIER',
            '47': '9SEATCARRIER',
            '48': 'SUV'
            }


def check_loc(loc):
    locationcode = ''
    while locationcode == '':
        parloc = dict(callback='',
                      msg='{"@xmlns":"http://www.cartrawler.com/","@Version":"1.000","@Target":"Production",'
                          '"@PrimaryLangID":"RU","POS":{"Source":[{"@ERSP_UserID":"AJ","@ISOCurrency":"RUB",'
                          '"RequestorID":{"@Type":"16","@ID":"463558","@ID_Context":"CARTRAWLER"},"@ISOCountry":"RU",'
                          '"@TerminalID":"RU"},{"RequestorID":{"@Type":"16","@ID":"0824165805012014905","@ID_Context":'
                          '"CUSTOMERID"}},{"RequestorID":{"@Type":"16","@ID":"0831200838014911014","@ID_Context":'
                          '"ENGINELOADID"}},{"RequestorID":{"@Type":"16","@ID":"463558","@ID_Context":"PRIORID"}},'
                          '{"RequestorID":{"@Type":"16","@ID":"3","@ID_Context":"BROWSERTYPE"}}]},'
                          '"VehLocSearchCriterion":{"@ExactMatch":"true","@ImportanceType":"Mandatory",'
                          '"PartialText":{"@Sort":"1","@POITypes":"","@Size":2000,"#text":"' + loc + '"}}}',
                      type='CT_VehLocSearchRQ')

        resloc = session.request('get', url, params=parloc)
        qlocation = json.loads(resloc.text)

        if 'Errors' in qlocation:
            print 'There are no records for the current settings'
            loc = raw_input('Enter location\n')
        elif '@Name' in qlocation['VehMatchedLocs']['LocationDetail']:
            print qlocation['VehMatchedLocs']['LocationDetail']['@Name']
            locationcode = qlocation['VehMatchedLocs']['LocationDetail']['@Code']
        else:
            print 'Found more than one location'
            locations = qlocation['VehMatchedLocs']['LocationDetail']
            for location in locations:
                print location['@Name']
            loc = raw_input('Enter pick-up location\n')
    return locationcode


def check_data(data):
    check = False
    while not check:
        try:
            int(data[:2])
            int(data[3:5])
        except ValueError:
            print 'Incorrect date format'
            data = raw_input('Enter the date in the format dd.mm.yyyy\n')
        else:
            if len(data) == 10 and data[2] == '.' and data[5] == '.' and int(data[:2]) <= 31 and int(data[3:5]) <= 12:
                check = True
            else:
                print 'Incorrect date format'
                data = raw_input('Enter the date in the format dd.mm.yyyy\n')
    return data


def check_time(time):
    check = False
    while not check:
        try:
            int(time[:2])
            int(time[3:])
        except ValueError:
            print 'Incorrect time format'
            time = raw_input('Enter the time in the format hh.mm\n')
        else:
            if len(time) == 5 and time[2] == '.' and int(time[:2]) < 24 and int(time[3:5]) < 60:
                check = True
            else:
                print 'Incorrect time format'
                time = raw_input('Enter the time in the format hh.mm\n')
    return time


url = 'http://otageo.cartrawler.com/cartrawlerota/json'

session = requests.session()

PickLocCode = check_loc(raw_input('Enter pick-up location\n'))
ReturnLocCode = check_loc(raw_input('Enter return location\n'))

PickDate = check_data(raw_input('Enter the date of pick-up in the format dd.mm.yyyy\n'))
PickTime = check_time(raw_input('Enter the time of pick-up in the format hh.mm\n'))
ReturnDate = check_data(raw_input('Enter the date of return in the format dd.mm.yyyy\n'))
ReturnTime = check_time(raw_input('Enter the time in the format hh.mm\n'))

PickUpDateTime = PickDate[6:10]+'-'+PickDate[3:5]+'-'+PickDate[0:2]+'T'+PickTime[0:2]+':'+PickTime[3:]+':00'
ReturnDateTime = ReturnDate[6:10]+'-'+ReturnDate[3:5]+'-'+ReturnDate[0:2]+'T'+ReturnTime[0:2]+':'+ReturnTime[3:]+':00'

print 'Loading list of cars'

parDateTime = {
    'callback':	'',
    'msg':	'{"@Target":"Production","@PrimaryLangID":"ru","POS":{"Source":[{"@ERSP_UserID":"AJ","@ISOCurrency":"RUB",'
    '"RequestorID":{"@Type":"16","@ID":"463558","@ID_Context":"CARTRAWLER"}},'
    '{"RequestorID":{"@Type":"16","@ID":"0831184104006863013","@ID_Context":"ENGINELOADID"}},'
    '{"RequestorID":{"@Type":"16","@ID":"463558","@ID_Context":"PRICEID"}},'
    '{"RequestorID":{"@Type":"16","@ID":"463558","@ID_Context":"PRIORID"}},'
    '{"RequestorID":{"@Type":"16","@ID":"3","@ID_Context":"BROWSERTYPE"}}]},'
    '"@xmlns":"http://www.opentravel.org/OTA/2003/05","@xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance",'
    '"@Version":"1.005","VehAvailRQCore":{"@Status":"Available","VehRentalCore":{"@PickUpDateTime":"'
    + PickUpDateTime + '","@ReturnDateTime":"' + ReturnDateTime + '","PickUpLocation":{"@CodeContext":'
    '"CARTRAWLER","@LocationCode":"'+PickLocCode+'"},"ReturnLocation":{"@CodeContext":"CARTRAWLER","'
    '@LocationCode":"'+ReturnLocCode+'"}},"VehPrefs":{"VehPref":{"VehClass":{"@Size":"0"}}},"DriverType":'
    '{"@Age":30}},"VehAvailRQInfo":{"Customer":{"Primary":{"CitizenCountryName":{"@Code":"DE"}}},'
    '"TPA_Extensions":{"showBaseCost":true,"Window":{"@name":"%u0414%u0435%u0448%u0435%u0432%u044B%u0435%20'
    '%u041F%u0440%u043E%u043A%u0430%u0442%20%u0430%u0432%u0442%u043E%u043C%u043E%u0431%u0438%u043B%u0435%'
    'u0439%3A%20%u0421%u0440%u0430%u0432%u043D%u0435%u043D%u0438%u0435%20%u041F%u0440%u043E%u043A%u0430%u0442%'
    '20%u0430%u0432%u0442%u043E%u043C%u043E%u0431%u0438%u043B%u0435%u0439%20%u0438%20%u0430%u0440%u0435%u043D%'
    'u0434%u0430%20%u0430%u0432%u0442%u043E%20%u0432%20Holiday%20Autos","@engine":"CTABE-V5.0","@svn":"5.0.35",'
    '"@region":"ru","UserAgent":"Mozilla/5.0 (Windows NT 6.3; WOW64)AppleWebKit/537.36 (KHTML, like Gecko)'
    ' Chrome/44.0.2403.157 Safari/537.36","BrowserName":"chrome","BrowserVersion":"44",'
    '"URL":"https://www.holidayautos.com/ru/book?c=ru&amp;clientId=463558&clientID=463558&ln=RU&elID=0831184104'
    '006863013&countryID=DE&pickupID=2864&returnID=2864&pickupName=%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0%20-'
    '%20%D0%9B%D0%B5%D0%BD%D0%B8%D0%BD%D0%B3%D1%80%D0%B0%D0%B4%D1%81%D0%BA%D0%B8%D0%B9%20%D0%B2%D0%BE%D0%BA%D0'
    '%B7%D0%B0%D0%BB&returnName=%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0%20-%20%D0%9B%D0%B5%D0%BD%D0%B8%D0%BD%D0%B'
    '3%D1%80%D0%B0%D0%B4%D1%81%D0%BA%D0%B8%D0%B9%20%D0%B2%D0%BE%D0%BA%D0%B7%D0%B0%D0%BB&pickupDateTime='
    + PickUpDateTime + '&returnDateTime=' + ReturnDateTime + '&age=30&curr=RUB&carGroupID=0&residenceID='
    'RU&CT=AJ&referrer=0:&c=ru"}}}}',
    'type':	'OTA_VehAvailRateRQ'
    }

autos = []
resCars = session.request('get', url, params=parDateTime)
qCars = json.loads(resCars.text)

if 'Errors' in qCars:
    print 'There are no records for the current settings'
elif 'VehVendorAvail' in qCars['VehAvailRSCore']['VehVendorAvails']:
    brCars = qCars['VehAvailRSCore']['VehVendorAvails']['VehVendorAvail']['VehAvails']
    for branch in brCars:
        autos.append(Auto(mark_model=branch['VehAvailCore']['TPA_Extensions']['VehMakeModel']['@Name'],
                          class_auto=branch['VehAvailCore']['Vehicle']['VehClass']['@Size']))
else:
    for branch in qCars['VehAvailRSCore']['VehVendorAvails']:
        for lowbr in branch['VehAvails']:
            autos.append(Auto(mark_model=lowbr['VehAvailCore']['TPA_Extensions']['VehMakeModel']['@Name'],
                              class_auto=lowbr['VehAvailCore']['Vehicle']['VehClass']['@Size']))

for auto in autos:
    print auto.mark_model + ' - ' + vehClass[auto.class_auto]
